-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2019 at 12:09 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inigza`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountandorder`
--

CREATE TABLE `accountandorder` (
  `Account_ID` int(11) NOT NULL,
  `Product_ID` int(11) NOT NULL,
  `Order_Id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bankinformation`
--

CREATE TABLE `bankinformation` (
  `bank_number` varchar(255) NOT NULL,
  `pin_code` varchar(255) NOT NULL,
  `bank_id` bigint(11) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`Id`, `Name`) VALUES
(1, 'Women'),
(2, 'Men'),
(3, 'Shirt'),
(4, 'Dress');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `Id` int(11) NOT NULL,
  `color_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`Id`, `color_name`) VALUES
(2, '#7fe68a'),
(1, '#ffffff');

-- --------------------------------------------------------

--
-- Table structure for table `item_order`
--

CREATE TABLE `item_order` (
  `order_id` bigint(20) NOT NULL,
  `product_id_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `payment` double NOT NULL,
  `size` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_order`
--

INSERT INTO `item_order` (`order_id`, `product_id_name`, `username`, `amount`, `order_status`, `payment`, `size`, `color`, `date`) VALUES
(34, 'shoe1congo', 'sena', 2, 'paymentPending', 100, 1, 2, '2019-02-16 20:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `Id` int(11) NOT NULL,
  `product_id_name` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Purchasing_price` double NOT NULL,
  `selling_price` double NOT NULL,
  `material_type` varchar(200) NOT NULL,
  `Description` longtext NOT NULL,
  `Category` varchar(255) NOT NULL,
  `Date` date NOT NULL,
  `gender` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`Id`, `product_id_name`, `Name`, `Purchasing_price`, `selling_price`, `material_type`, `Description`, `Category`, `Date`, `gender`) VALUES
(1, 'shoe1congo', 'congo', 78.9, 100, 'goma', 'kjdshjkfkhajdkhkadsfhgakgdhfkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhfkjdshjkfkhajdkhkadsfhgakgdhfkjdshjkfkhajdkhkadsfhgakgdhf\r\nkjdshjkfkhajdkhkadsfhgakgdhf', '5', '2019-02-20', 'f'),
(2, 'jebena', 'Jebena', 34, 50, 'clay soil', ' djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjhdjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjhdjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh\r\ndjajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh djajkhdjh', 'house material', '2019-02-21', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `product_color_size`
--

CREATE TABLE `product_color_size` (
  `color_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `product_id_name` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `amount_in_stock` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `selling_price` double NOT NULL,
  `free_shipping` varchar(20) NOT NULL,
  `initial_selling_price` double NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_color_size`
--

INSERT INTO `product_color_size` (`color_id`, `size_id`, `product_id_name`, `amount`, `amount_in_stock`, `Image`, `selling_price`, `free_shipping`, `initial_selling_price`, `category_id`) VALUES
(2, 1, 'shoe1congo', 34, 4, '', 33, 'yes', 5, 3),
(2, 2, 'jebena', 29, 33, '', 100, 'yes', 77.9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE `size` (
  `Id` int(11) NOT NULL,
  `size_type` varchar(200) NOT NULL,
  `unit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`Id`, `size_type`, `unit`) VALUES
(1, 'small', 'mm'),
(2, '39', 'cm');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `optional_phone_number` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `phone_number`, `email`, `username`, `password`, `optional_phone_number`, `enabled`, `first_name`, `last_name`) VALUES
(1, '0933910176', 'eden.abdisa@yahoo.com', 'ed', '$2a$10$Ag92bezZvyCkb6gX580Ueu/RWgIkjP1yPYBuQUSkiDEYMSNcEKUni', '0933910176', 1, 'eden', 'abdisa'),
(3, '0933910176', 'eden.abdisa@yahoo.com', 'amenSeme', '$2a$10$qPvq1PvSTlTemGa6XCMt8O1ZXoJ6KKO/qaQjkrIJE8T2qykcGr77q', '0933910176', 1, 'amen', 'seme'),
(4, '0933910176', 'eden.abdisa@yahoo.com', 'ab', '$2a$10$kxv6nCmWJBFHlIK84ga68OORU6I3tRwpJk6wsPP38uj.u4qBgW26W', '0933910176', 1, 'eden', 'abdisa'),
(5, '0933910176', 'eden.abdisa@yahoo.com', 'sena', '$2a$10$4j08XQGZ24KY3EcTwI6a1OTiDIGDhTs2SzfjyNTZ0AlsSYTUFMe2.', '0933910176', 1, 'eden', 'yt');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankinformation`
--
ALTER TABLE `bankinformation`
  ADD PRIMARY KEY (`bank_id`),
  ADD UNIQUE KEY `bank_number` (`bank_number`),
  ADD KEY `fk_l_id` (`user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `color_name` (`color_name`);

--
-- Indexes for table `item_order`
--
ALTER TABLE `item_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `product_id_name` (`product_id_name`),
  ADD KEY `username` (`username`),
  ADD KEY `color` (`color`),
  ADD KEY `size` (`size`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `product_id_name` (`product_id_name`);

--
-- Indexes for table `product_color_size`
--
ALTER TABLE `product_color_size`
  ADD PRIMARY KEY (`color_id`,`size_id`,`product_id_name`),
  ADD KEY `size_id` (`size_id`),
  ADD KEY `product_id_name` (`product_id_name`),
  ADD KEY `product_color_size_ibfk_4` (`category_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankinformation`
--
ALTER TABLE `bankinformation`
  MODIFY `bank_id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `item_order`
--
ALTER TABLE `item_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bankinformation`
--
ALTER TABLE `bankinformation`
  ADD CONSTRAINT `fk_l_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`username`);

--
-- Constraints for table `item_order`
--
ALTER TABLE `item_order`
  ADD CONSTRAINT `item_order_ibfk_1` FOREIGN KEY (`product_id_name`) REFERENCES `product` (`product_id_name`),
  ADD CONSTRAINT `item_order_ibfk_2` FOREIGN KEY (`username`) REFERENCES `user` (`username`),
  ADD CONSTRAINT `item_order_ibfk_3` FOREIGN KEY (`color`) REFERENCES `color` (`Id`),
  ADD CONSTRAINT `item_order_ibfk_4` FOREIGN KEY (`size`) REFERENCES `size` (`Id`);

--
-- Constraints for table `product_color_size`
--
ALTER TABLE `product_color_size`
  ADD CONSTRAINT `product_color_size_ibfk_1` FOREIGN KEY (`color_id`) REFERENCES `color` (`Id`),
  ADD CONSTRAINT `product_color_size_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `size` (`Id`),
  ADD CONSTRAINT `product_color_size_ibfk_3` FOREIGN KEY (`product_id_name`) REFERENCES `product` (`product_id_name`),
  ADD CONSTRAINT `product_color_size_ibfk_4` FOREIGN KEY (`category_id`) REFERENCES `category` (`Id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
