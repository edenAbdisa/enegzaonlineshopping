package enigza;


import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class EngizaMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngizaMainApplication.class, args);
		log.info("EnigzaMainApplication line15");
	}
}