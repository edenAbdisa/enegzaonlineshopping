package enigza.domains;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import enigza.security.User;
import lombok.Data;
	@Data
	@Entity
	@Table(name="bankinformation",schema="inigza")
	public class BankAccount {
		private User userId;
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private long bankId;		 
		
		@NotBlank(message = "Please provide a username")
		private String bankNumber;
		
		@Column(name="phone_number")
		@NotBlank(message="Phone number is required")
		private String pinCode;
				 
		
		@OneToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name = "user_id")
	    public User getUser() {
	        return userId;
	    }

	    public void setUser(User userId) {
	        this.userId = userId;
	    }
		
		
	}
