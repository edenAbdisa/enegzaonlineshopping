package enigza.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="color",schema="inigza")
public class Color {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Id;		 
	
	@Column(name="color_name")
	@NotBlank(message = "Please provide a colorName")
	private String color;
	
}
