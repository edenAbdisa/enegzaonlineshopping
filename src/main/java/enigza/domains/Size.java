package enigza.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="size",schema="inigza")
public class Size {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Id;		 
	
	@Column(name="size_type")
	@NotBlank(message = "Please provide a sizeType")
	private String sizeName;
	
	@Column(name="unit")
	@NotBlank(message = "Please provide a unit")
	private String unit;
	
	public static enum Type {
		SMALL, MEDIUM, LARGE, XL, XXL
	}
}
