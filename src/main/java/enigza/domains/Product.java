package enigza.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
@Data
@Entity
@Table(name="product",schema="inigza")
public class Product {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Id;		 
	
	@Column(name="product_id_name")
	private String productIdName;
	
	@Column(name="Name")
	@NotBlank(message="Item name is required")
	private String Name;
	
	@Column(name="Purchasing_price")
	@NotBlank(message="PurchasingPrice is required")
	private double PurchasingPrice;
	
	@Column(name="selling_price")
	@NotBlank(message="sellingPrice is required")
	private double sellingPrice;
	
	@Column(name="material_type")
	@NotBlank(message="materialType is required")
	private String materialType;
	
	@Column(name="Description")
	@NotBlank(message="Description is required")
	private String Description;
	
	@Column(name="Category")
	@NotBlank(message="Category is required")
	private String Category;
	
	@Column(name="gender")
	@NotBlank(message="gender is required")
	private String gender;
	
}
