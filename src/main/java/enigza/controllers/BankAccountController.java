package enigza.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import enigza.domains.BankAccount;
import enigza.security.User;
import enigza.services.BankAccountService;
import enigza.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Controller 
@Slf4j
public class BankAccountController {
	 @Autowired
	  private BankAccountService bankService;
		
	 @ModelAttribute(name = "bank")
		public BankAccount bank() {
			return new BankAccount();
		} 
	 @GetMapping("/addBankInfo")     // <2>
	  public String contactPage() {
	    return "addBankInfo";     // <3>
	  } 
		@PostMapping("/createBankAccount") // <2>
		public String createAccount(@Valid BankAccount bank, BindingResult bindingResult, Model model) {
			//User accountInfoExist = userService.findUserByUsername(user.getPhoneNumber());
			if (bank != null) {
				bindingResult.rejectValue("account", "error.user",
						"There is already an account registered with the values entered.");
			}
			if (bindingResult.hasErrors()) {
				return "account";
			} else {
				BankAccount bankAccountSaved=bankService.save(bank);
				model.addAttribute("displayedWhenAccountIsSuccesfullyCreated", "User has been created successfully");
				return "login";
			}
		}
}
