package enigza.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.support.SessionStatus;

import enigza.domains.BankAccount;
import enigza.security.User;
import enigza.services.ColorService;
import enigza.services.ProductService;
import enigza.services.ProductSizeColorService;
import enigza.services.RoleService;
import enigza.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j // <1>
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	
	@Autowired
	  public UserController(UserService userService,RoleService roleService) {
		  this.userService=userService;
		  this.roleService=roleService;
	  }
	@ModelAttribute(name = "account")
	public User user() {
		return new User();
	} 
	@ModelAttribute(name="bank")
		public BankAccount bankAccount() {
			return new BankAccount();
		}
	@GetMapping("/login")     // <2>
	  public String LogIn() {
	    return "login";     // <3>
	 }
	@PostMapping("/LogInAccount")
	public String LogInAccount() {
		return "contact";
	}
	@GetMapping("/account")
	  public String AccountPage() {
		
	    return "account";    
	  }
	@PostMapping("/createAccount") // <2>
	public String createAccount(@Valid User account, BindingResult bindingResult, Model model) {
		User accountInfoExist = userService.findUserByUsername(account.getUsername());
		if (accountInfoExist != null) {
			bindingResult.rejectValue("account", "error.user",
					"There is already an account registered with the values entered.");
		}
		if (bindingResult.hasErrors()) {
			model.addAttribute("successMessage", bindingResult.getAllErrors());
			return "account";
		} else {
            userService.saveUser(account);
            model.addAttribute("successMessage", "User has been registered successfully");
            
			//model.addAttribute("displayedWhenAccountIsSuccesfullyCreated", "User has been created successfully");
			return "home";
		}
		
	}
	@GetMapping("/access-denied")
	public String accessDenied() {
		return "access_denied";
	}


}
