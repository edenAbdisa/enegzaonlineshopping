package enigza.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import enigza.domains.BankAccount;
import enigza.domains.Product;
import enigza.security.User;
import enigza.services.BankAccountService;
import enigza.services.ProductService;
import enigza.services.ProductSizeColorService;
import enigza.services.UserService;
import lombok.extern.slf4j.Slf4j;
@Controller 
@Slf4j

public class HomeController {
	  
	  private ProductSizeColorService productSizeColor;
	  
	  @Autowired
	  public HomeController(ProductSizeColorService productSizeColor) {
		  
		  this.productSizeColor=productSizeColor;
	  }
	 
  @GetMapping("/")     // <2>
  public String home(Model model) {
	  model.addAttribute("ListOfProduct",productSizeColor.findProductSizeColorByAmountInStockGreaterThan(0));
    return "home";     // <3>
  }
  
  @GetMapping("/contact")     // <2>
  public String contactPage() {
    return "contact";     // <3>
  }  
	
  @GetMapping("/testsec")
  public String testSec() {
    return "testsec";    
  }
  
}
