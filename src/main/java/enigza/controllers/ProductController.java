package enigza.controllers;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.support.SessionStatus;

import enigza.domains.BankAccount;
import enigza.domains.Product;
import enigza.repositories.ProductRepository;
import enigza.security.User;
import enigza.services.ProductService;
import enigza.services.UserService;
import lombok.extern.slf4j.Slf4j;
public class ProductController {
	@Autowired
	private ProductService productService;
	
	private ProductRepository productRepos;
	
	public ProductController(ProductService productService) {
		
		this.productService = productService;
	}
	@GetMapping("/addProduct")
	  public String AccountPage() {
	    return "addProduct";    
	  }
	@PostMapping
	public String addProduct(@Valid Product product,Errors errors) {
		if(errors.hasErrors()) {
			return "addProduct";
		}
		this.productService.save(product);
		return "redirect:/";
		
	}
	
}
