package enigza.controllers;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import enigza.domains.Color;
import enigza.domains.Order;
import enigza.domains.Product;
import enigza.domains.ProductSizeColor;
import enigza.domains.Size;
import enigza.services.ColorService;
import enigza.services.ProductService;
import enigza.services.ProductSizeColorService;
import enigza.services.SizeService;
@Controller
public class singleController {
	private ProductSizeColorService productSizeColor;
	private ProductService product ;
	private ColorService colorService;
	private SizeService sizeService;
	@Autowired
	public singleController(ProductSizeColorService productSizeColor,ColorService colorService,ProductService product,SizeService sizeService) {
		  this.product=product;
		  this.productSizeColor=productSizeColor;
		  this.colorService=colorService;
		  this.sizeService=sizeService;
	}
	  @ModelAttribute(name = "order")
	  public Order order() {
			return new Order();
		}
	  @GetMapping("/single/{id}")     // <2>
	  public String singlePage(@PathVariable String id,Model model) {
		List<Color> color = new ArrayList<>();
		List<Size> size = new ArrayList<>();
		ProductSizeColor pro=productSizeColor.findByProductIdName(id).get(0);
		Product singleProduct= product.findByProductIdName(id).get(0);
		model.addAttribute("singleProductSizeColor",pro );
		model.addAttribute("singleProduct",singleProduct );
		
		productSizeColor.findByProductIdName(id)
						.forEach( i->color.add(colorService.findById(i.getColorId()).get()));
		for (Color type: color) {
			size.clear();
			productSizeColor.findByProductIdNameAndColorId(pro.getProductIdName(), type.getId())
							.forEach(i->size.add(sizeService.findById(i.getSizeId()).get()));
			model.addAttribute(String.valueOf( type.getId()), size);
		}
		model.addAttribute("colorList",color);
	    return "single";     // <3>
	  } 
	  

}
