package enigza.repositories;

import org.springframework.data.repository.CrudRepository;

import enigza.domains.Order;
 
public interface OrderRepository extends CrudRepository<Order, Long> {
}
