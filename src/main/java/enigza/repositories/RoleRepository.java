package enigza.repositories;

import org.springframework.data.repository.CrudRepository;

import enigza.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	 Role findByRole(String role);
	 
}
