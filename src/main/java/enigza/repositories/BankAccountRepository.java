package enigza.repositories;

import org.springframework.data.repository.CrudRepository;

import enigza.domains.BankAccount;
import enigza.security.Role;

public interface BankAccountRepository extends CrudRepository<BankAccount, Long>{

	BankAccount findByBankNumber(String bank);
	 
}
