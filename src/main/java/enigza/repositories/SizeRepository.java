package enigza.repositories;

import org.springframework.data.repository.CrudRepository;

import enigza.domains.Size;

public interface SizeRepository extends CrudRepository<Size, Long>{

	Size findBySizeName(String sizeName);
	 
}
