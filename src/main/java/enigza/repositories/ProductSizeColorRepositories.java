package enigza.repositories;
<<<<<<< HEAD

import org.springframework.data.repository.CrudRepository;

import enigza.domains.ProductSizeColor;

public interface ProductSizeColorRepositories extends CrudRepository<ProductSizeColor, Long>{

	ProductSizeColor findByProductIdName(String ProductSizeColor);
=======
 
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import enigza.domains.ProductSizeColor;

public interface ProductSizeColorRepositories extends CrudRepository<ProductSizeColor, Long>{
	ProductSizeColor findByProductIdNameAndColorIdAndSizeId(String ProductSizeColor,long colorId,long sizeId);
	List<ProductSizeColor> findByProductIdNameAndColorId(String ProductSizeColor,long colorId);
	List<ProductSizeColor> findByProductIdName(String ProductSizeColor);
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE ProductSizeColor c SET c.amountInStock = :amountInStock WHERE c.colorId = :colorId AND c.sizeId= :sizeId AND c.productIdName= :productIdName")
    int updateAmountInStock(@Param("productIdName") String productIdName,
    		@Param("colorId") long colorId,
    		@Param("sizeId") long sizeId,
    		@Param("amountInStock") long amountInStock);
	//@Query("ProductSizeColor p where p.amountInStock >0")
	List<ProductSizeColor> findProductSizeColorByAmountInStockGreaterThan(long value);
>>>>>>> e24094d5894ee598c7b07958ef957fd6df757e96
	 
}
