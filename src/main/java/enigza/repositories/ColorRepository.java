package enigza.repositories;

import org.springframework.data.repository.CrudRepository;

import enigza.domains.Color;


public interface ColorRepository extends CrudRepository<Color, Long>{

	Color findByColor(Color color);
	 
}

