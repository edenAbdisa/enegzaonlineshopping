package enigza.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import enigza.security.User;

 
public interface AccountRepositories extends CrudRepository<User, Long>{
	User findByUsername(String userName);
	
}
