package enigza.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import enigza.domains.Product;
import enigza.domains.ProductSizeColor;
import enigza.security.User;

 
public interface ProductRepository extends CrudRepository<Product, Long>{

	List<Product> findByProductIdName(String Product);
	 
}

