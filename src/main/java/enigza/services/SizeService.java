package enigza.services;

import java.util.Optional;

import enigza.domains.Size;
import enigza.security.Role;

public interface SizeService {
    public Size save(Size role);
	
	public Iterable<Size> saveAll(Iterable<Size> color);

	Optional<Size> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Size> findAll();

	Iterable<Size> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Size role);
	
	void deleteAll(Iterable<Size> roles);

	void deleteAll();
	Size findBySizeName(String sizeName);
}
