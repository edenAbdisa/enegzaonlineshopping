package enigza.services;

import org.springframework.security.core.userdetails.UserDetailsService;


import enigza.security.User;

public interface UserService extends UserDetailsService {

	User findUserByUsername(String userName);
	void saveUser(User user);
	
}
