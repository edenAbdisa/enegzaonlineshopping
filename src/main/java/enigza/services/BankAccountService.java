package enigza.services;

import java.util.Optional;

import enigza.domains.BankAccount;

public interface BankAccountService {
	
    public BankAccount save(BankAccount role);
	
	public Iterable<BankAccount> saveAll(Iterable<BankAccount> roles);

	Optional<BankAccount> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<BankAccount> findAll();

	Iterable<BankAccount> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(BankAccount role);
	
	void deleteAll(Iterable<BankAccount> roles);

	void deleteAll();
}
