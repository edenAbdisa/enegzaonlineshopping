package enigza.services;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import enigza.domains.BankAccount;
import enigza.repositories.BankAccountRepository;

@Service
public class BankAccountServiceImpl implements BankAccountService {
	
	BankAccountRepository roleRepository;
	
	@Autowired
	public BankAccountServiceImpl( BankAccountRepository roleRepository) {
		this.roleRepository = roleRepository;
	}
	@Override
	public BankAccount save(BankAccount role) {
		return roleRepository.save(role);
	}

	@Override
	public Iterable<BankAccount> saveAll(Iterable<BankAccount> roles) {
		return roleRepository.saveAll(roles);
	}

	@Override
	public Optional<BankAccount> findById(Long id) {
		return roleRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return roleRepository.existsById(id);
	}

	@Override
	public Iterable<BankAccount> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Iterable<BankAccount> findAllById(Iterable<Long> ids) {
		return roleRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return roleRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		roleRepository.deleteById(id);
	}

	@Override
	public void delete(BankAccount role) {
		roleRepository.delete(role);
	}

	@Override
	public void deleteAll(Iterable<BankAccount> roles) {
		roleRepository.deleteAll(roles);
	}

	@Override
	public void deleteAll() {
		roleRepository.deleteAll();
	}

}

