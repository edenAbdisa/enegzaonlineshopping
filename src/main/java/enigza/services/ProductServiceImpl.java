package enigza.services;

<<<<<<< HEAD
import java.util.Optional;

import enigza.domains.Product;
import enigza.security.Role;

=======
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import enigza.domains.Product;
import enigza.repositories.ProductRepository;
import enigza.repositories.ProductSizeColorRepositories;
import enigza.security.Role;
@Service
>>>>>>> e24094d5894ee598c7b07958ef957fd6df757e96
public class ProductServiceImpl implements ProductService{
    ProductService productService;
	@Override
	public Product save(Product product) {
		return this.productService.save(product);
	}

	@Override
	public Iterable<Product> saveAll(Iterable<Product> product) {
		return this.productService.saveAll(product);
	}

	@Override
	public Optional<Product> findById(Long id) {
		return this.productService.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.productService.existsById(id);
	}

	@Override
	public Iterable<Product> findAll() {
		return this.findAll();
	}

	@Override
	public Iterable<Product> findAllById(Iterable<Long> ids) {
		return this.productService.findAllById(ids);
	}

	@Override
	public long count() {
		return this.productService.count();
	}

	@Override
	public void deleteById(Long id) {
		this.deleteById(id);
		
	}

	@Override
	public void delete(Product product) {
		this.productService.delete(product);
		
	}

	@Override
	public void deleteAll(Iterable<Product> product) {
		this.productService.deleteAll(product);
		
	}

	@Override
	public void deleteAll() {
		this.deleteAll();
		
	}
	
	ProductRepository  productRepository;
	public ProductServiceImpl(ProductRepository  productRepository) {
		this.productRepository=productRepository;
	}
	
	@Override
	public Product save(Product product) {
		return this.productRepository.save(product);
	}

	@Override
	public Iterable<Product> saveAll(Iterable<Product> product) {
		return this.productRepository.saveAll(product);
	}

	@Override
	public Optional<Product> findById(Long id) {
		return this.productRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.productRepository.existsById(id);
	}

	@Override
	public Iterable<Product> findAll() {
		return this.productRepository.findAll();
	}

	@Override
	public Iterable<Product> findAllById(Iterable<Long> ids) {
		return this.productRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return this.productRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		this.productRepository.deleteById(id);
		
	}

	@Override
	public void delete(Product product) {
		this.productRepository.delete(product);
		
	}

	@Override
	public void deleteAll(Iterable<Product> product) {
		this.productRepository.deleteAll(product);
		
	}

	@Override
	public void deleteAll() {
		this.productRepository.deleteAll();
		
	}

	@Override
	public List<Product> findByProductIdName(String Product) {
		return this.productRepository.findByProductIdName(Product);		
	}
	

}
