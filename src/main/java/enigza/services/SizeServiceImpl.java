package enigza.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import enigza.domains.Size;
import enigza.repositories.SizeRepository;

@Service
public class SizeServiceImpl implements SizeService{

	SizeRepository sizeRepository;
	@Autowired
	public SizeServiceImpl( SizeRepository colorRepository) {
		this.sizeRepository = colorRepository;
	}
	
	@Override
	public Size save(Size role) {
		return sizeRepository.save(role);
	}

	@Override
	public Iterable<Size> saveAll(Iterable<Size> size) {
		return sizeRepository.saveAll(size);
	}

	@Override
	public Optional<Size> findById(Long id) {
		return sizeRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return sizeRepository.existsById(id);
	}

	@Override
	public Iterable<Size> findAll() {
		return sizeRepository.findAll();
	}

	@Override
	public Iterable<Size> findAllById(Iterable<Long> ids) {
		return sizeRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return sizeRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		sizeRepository.deleteById(id);
		
	}

	@Override
	public void delete(Size role) {
		sizeRepository.delete(role);
		
	}

	@Override
	public void deleteAll(Iterable<Size> roles) {
		sizeRepository.deleteAll(roles);
		
	}

	@Override
	public void deleteAll() {
		sizeRepository.deleteAll();
		
	}

	@Override
	public Size findBySizeName(String sizeName) {
		return this.sizeRepository.findBySizeName(sizeName);
	}
	
	
	
}
