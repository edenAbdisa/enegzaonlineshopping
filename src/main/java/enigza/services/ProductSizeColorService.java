package enigza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import enigza.domains.ProductSizeColor;

public interface ProductSizeColorService {
public ProductSizeColor save(ProductSizeColor role);
	
	public Iterable<ProductSizeColor> saveAll(Iterable<ProductSizeColor> color);

	Optional<ProductSizeColor> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<ProductSizeColor> findAll();

	Iterable<ProductSizeColor> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(ProductSizeColor role);
	
	void deleteAll(Iterable<ProductSizeColor> roles);

	void deleteAll();
<<<<<<< HEAD
	ProductSizeColor findByProductIdName(String ProductSizeColor);
=======
	List<ProductSizeColor> findByProductIdName(String ProductSizeColor);
	List<ProductSizeColor> findByProductIdNameAndColorId(String ProductSizeColor,long colorId);
	List<ProductSizeColor> findProductSizeColorByAmountInStockGreaterThan(long value);
	ProductSizeColor findByProductIdNameAndColorIdAndSizeId(String ProductSizeColor,long colorId,long sizeId);
	 int updateAmountInStock(@Param("productIdName") String productIdName,
	    		@Param("colorId") long colorId,
	    		@Param("sizeId") long sizeId,
	    		@Param("amountInStock") long amountInStock);
>>>>>>> e24094d5894ee598c7b07958ef957fd6df757e96
}
