package enigza.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
=======
import org.springframework.stereotype.Service;
>>>>>>> e24094d5894ee598c7b07958ef957fd6df757e96

import enigza.domains.BankAccount;
import enigza.domains.Color;
import enigza.repositories.BankAccountRepository;
import enigza.repositories.ColorRepository;
import enigza.security.Role;
<<<<<<< HEAD

=======
@Service
>>>>>>> e24094d5894ee598c7b07958ef957fd6df757e96
public class ColorServiceImpl implements ColorService{

	ColorRepository colorRepository;
	
	@Autowired
	public ColorServiceImpl( ColorRepository colorRepository) {
		this.colorRepository = colorRepository;
	}
	@Override
	public Color save(Color role) {
		return colorRepository.save(role);
	}

	@Override
	public Iterable<Color> saveAll(Iterable<Color> color) {
		return colorRepository.saveAll(color);
	}

	@Override
	public Optional<Color> findById(Long id) {
		return colorRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return colorRepository.existsById(id);
	}

	@Override
	public Iterable<Color> findAll() {
		return colorRepository.findAll();
	}

	@Override
	public Iterable<Color> findAllById( Iterable<Long> ids) {
		return colorRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return colorRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		colorRepository.deleteById(id);
	}

	@Override
	public void delete(Color role) {
		colorRepository.delete(role);
	}

	@Override
	public void deleteAll(Iterable<Color> roles) {
		colorRepository.deleteAll(roles);
	}

	@Override
	public void deleteAll() {
		colorRepository.deleteAll();
	}

}