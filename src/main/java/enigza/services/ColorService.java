package enigza.services;

import java.util.Optional;

import enigza.domains.Color;
import enigza.security.Role;

public interface ColorService {
    public Color save(Color role);
	
	public Iterable<Color> saveAll(Iterable<Color> color);

	Optional<Color> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Color> findAll();

	Iterable<Color> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Color role);
	
	void deleteAll(Iterable<Color> roles);

	void deleteAll();
}
