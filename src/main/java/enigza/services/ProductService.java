package enigza.services;

import java.util.List;
import java.util.Optional;

import enigza.domains.Color;
import enigza.domains.Product;
import enigza.security.Role;

public interface ProductService {
public Product save(Product role);
	
	public Iterable<Product> saveAll(Iterable<Product> color);

	Optional<Product> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Product> findAll();

	Iterable<Product> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Product product);
	
	void deleteAll(Iterable<Product> product);

	void deleteAll();
	List<Product> findByProductIdName(String Product);
}
