package com.AADE.inigza.services;

import java.util.Iterator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.*;
import com.AADE.inigza.respositories.*;

import antlr.collections.List;;

 
@Service
public class OrderServiceImpl implements OrderService {
	
	OrderRepository orderRepository;
	
	@Autowired
	public OrderServiceImpl(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	@Override
	public Order save(Order order) {
		return orderRepository.save(order);
	}

	@Override
	public Iterable<Order> saveAll(Iterable<Order> orders) {
		return orderRepository.saveAll(orders);
	}

	@Override
	public Optional<Order> findById(Long id) {
		return orderRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return orderRepository.existsById(id);
	}

	@Override
	public Iterable<Order> findAll() {
		return orderRepository.findAll();
	}

	@Override
	public Iterable<Order> findAllById(Iterable<Long> ids) {
		return orderRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return orderRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		orderRepository.deleteById(id);
	}

	@Override
	public void delete(Order order) {
		orderRepository.delete(order);
	}

	@Override
	public void deleteAll(Iterable<Order> orders) {
		orderRepository.deleteAll(orders);

	}

	@Override
	public void deleteAll() {
		orderRepository.deleteAll();
	}

	@Override
	public java.util.List<Order> findByOrderStatus(String orderStatus) {
		// TODO Auto-generated method stub
		return orderRepository.findByOrderStatus(orderStatus);
	}

	@Override
	public java.util.List<Order> findByUserName(String userName) {
		// TODO Auto-generated method stub
		return orderRepository.findByUserName(userName);
	}

}
