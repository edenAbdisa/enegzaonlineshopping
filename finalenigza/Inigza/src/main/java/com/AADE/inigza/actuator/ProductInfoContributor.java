package com.AADE.inigza.actuator;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import com.AADE.inigza.respositories.ProductRepository;

@Component
public class ProductInfoContributor implements InfoContributor {

  private ProductRepository tacoRepo;

  public ProductInfoContributor(ProductRepository tacoRepo) {
    this.tacoRepo = tacoRepo;
  }

  @Override
  public void contribute(Builder builder) {
    long tacoCount = tacoRepo.count();
    Map<String, Object> tacoMap = new HashMap<String, Object>();
    tacoMap.put("count", tacoCount);
    builder.withDetail("product-stats", tacoMap);
  }

}
