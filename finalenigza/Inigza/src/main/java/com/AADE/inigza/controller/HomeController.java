package com.AADE.inigza.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.AADE.inigza.domain.Product;

@Controller

public class HomeController {
	
	@GetMapping("/")
	public String home(Model model) {
		Date date = new Date();
		date.toInstant();
		model.addAttribute("pdts", new Product());
		return "home";
	}
	
	@GetMapping("/user/home")
	public String userHome(Model model,Principal principal) {
		if(principal!=null) {
		model.addAttribute("username",principal.getName());
		}
		return "home_user";
	}
	
	

}
