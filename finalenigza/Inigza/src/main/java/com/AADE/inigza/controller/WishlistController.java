package com.AADE.inigza.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.AADE.inigza.domain.Order;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.Wishlist;
import com.AADE.inigza.domain.WishlistAndCartHelper;
import com.AADE.inigza.security.User;
import com.AADE.inigza.services.ProductService;
import com.AADE.inigza.services.UserService;
import com.AADE.inigza.services.WishlistService;

@Controller
@SessionAttributes("user")
public class WishlistController {

	@Autowired
	private WishlistService wishlistService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	
	public WishlistController(ProductService productService,WishlistService wishlistService,UserService userService) {
		this.wishlistService = wishlistService;
		this.userService = userService;
		this.productService = productService;
	}
	
	@GetMapping("/user/wishlist")
	public String showUserOrder(Model model,Principal principal) {
		List<User> userID = userService.findByUsername(principal.getName()); 
		List<Product> pdt = null;
		List<Wishlist> AllWishlist = wishlistService.findByUserID(userID.get(0).getId());
		for(int i=0;i<AllWishlist.size();i++) {
		 pdt =  productService.findByProductIdName(AllWishlist.get(i).getProductID());
		}		
		model.addAttribute("pdt",pdt);
		return "wishlist";
	}
	@GetMapping("/user/cart")
	public String showUserCart(Model model,Principal principal) {	
		List<User> userID = userService.findByUsername(principal.getName()); 
		List<Product> pdt = null;
		List<Wishlist> AllWishlist = wishlistService.findByUserID(userID.get(0).getId());
		for(int i=0;i<AllWishlist.size();i++) {
		 pdt =  productService.findByProductIdName(AllWishlist.get(i).getProductID());
		}		
		model.addAttribute("pdt",pdt);
		return "wishlist";
	}
	
}
