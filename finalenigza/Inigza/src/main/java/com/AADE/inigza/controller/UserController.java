package com.AADE.inigza.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.AADE.inigza.domain.Product;
import com.AADE.inigza.security.User;
import com.AADE.inigza.services.ProductService;
import com.AADE.inigza.services.UserService;

@Controller
@SessionAttributes("user")
public class UserController {

private final UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
		
	}
	
	@GetMapping("/allusers")
	public String showDashBoard(Model model) {
		
		List<User> AllUsers = (List<User>) userService.findAll();
		model.addAttribute("product",AllUsers);
		return "AdminProduct";
	}


	
	@GetMapping("/editusers")
	public String showEditProduct(Model model,@RequestParam("Id")Long id) {
		Optional<User> pd = userService.findById(id);
		model.addAttribute("product",pd);
		
		return "EditProduct";
	}
	
	@PostMapping("/updateusers")
	public String processEditProduct(@Valid User user) {	
		userService.saveUser(user);
		return "redirect:/all";
	}
	@GetMapping("/order/user")
	public String showUserOrder(Model model,Principal principal) {
		model.addAttribute("user",principal.getName());
		
		
		return "User";
	}
	
	
}
