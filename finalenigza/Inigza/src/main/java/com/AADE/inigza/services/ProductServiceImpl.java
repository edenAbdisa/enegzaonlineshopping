package com.AADE.inigza.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.security.*;

import com.AADE.inigza.respositories.ColorRepository;
import com.AADE.inigza.respositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	ProductRepository productRepository;
	ColorRepository colorRepository;
	
	@Autowired
	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
		this.colorRepository = colorRepository;
	}
	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Iterable<Product> saveAll(Iterable<Product> products) {
		return productRepository.saveAll(products);
	}

	@Override
	public Optional<Product> findById(int id) {
		return productRepository.findById(id);
	}

	@Override
	public boolean existsById(int id) {
		// TODO Auto-generated method stub
		return productRepository.existsById(id);
	}

	@Override
	public Iterable<Product> findAll() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public Iterable<Product> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return productRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return productRepository.count();
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		productRepository.deleteById(id);
	}

	@Override
	public void delete(Product product) {
		// TODO Auto-generated method stub
		productRepository.delete(product);
	}

	@Override
	public void deleteAll(Iterable<Product> products) {
		// TODO Auto-generated method stub
		productRepository.deleteAll(products);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		productRepository.deleteAll();
	}
	@Override
	public Iterable<Product> findByGender(String Gender) {
		Iterable<Product> pdt =  productRepository.findByGender(Gender);
		return pdt;
	}
	@Override
	public List<Product> findByProductIdName(String id) {
		// TODO Auto-generated method stub
		return productRepository.findByName(id);
	}




}
