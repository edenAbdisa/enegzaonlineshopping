package com.AADE.inigza;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication

@EnableAdminServer
public class InigzaApplication {

	public static void main(String[] args) {
		SpringApplication.run(InigzaApplication.class, args);
	}
	

}

