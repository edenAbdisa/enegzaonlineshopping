package com.AADE.inigza.services;

import java.util.Iterator;
import java.util.Optional;
import java.util.List;

import com.AADE.inigza.domain.Order;



 
public interface OrderService {

	public Order save(Order order);
	
	public Iterable<Order> saveAll(Iterable<Order> orders);

	Optional<Order> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Order> findAll();

	Iterable<Order> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Order order);
	
	void deleteAll(Iterable<Order> orders);

	void deleteAll();
	java.util.List<Order> findByOrderStatus(String orderStatus);
	java.util.List<Order> findByUserName(String userName);
}
