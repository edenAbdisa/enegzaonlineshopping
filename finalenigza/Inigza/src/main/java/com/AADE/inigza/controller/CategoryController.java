package com.AADE.inigza.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.AADE.inigza.domain.Product;
import com.AADE.inigza.services.ProductService;

@Controller
public class CategoryController {
	@Autowired
	private ProductService productService;
	
	public CategoryController(ProductService productService) {
		this.productService = productService;
	}
	
	
	@GetMapping("/categorieswomen")
	public String showWomenCategory(Product product,Model model) {	
		List<Product> AllProducts = (List<Product>) productService.findByGender("Female");
		model.addAttribute("pdts",AllProducts);
		return "categorieswomen";
	}
	@GetMapping("/categoriesmen")
	public String showMenCategory(Product product,Model model) {	
		List<Product> AllProducts = (List<Product>) productService.findByGender("Male");
	
		model.addAttribute("pdts",AllProducts);
		
		return "categoriesmen";
	}
	@GetMapping("/categoriesaccessory")
	public String showAccessCategory(Product product,Model model) {	
		List<Product> AllProducts = (List<Product>) productService.findByGender("Male");
		model.addAttribute("pdts",AllProducts);
		return "categoriesaccessory";
	}
	@GetMapping("/categoriestraditional")
	public String showTradCategory(Product product,Model model) {	
		List<Product> AllProducts = (List<Product>) productService.findByGender("Male");
		model.addAttribute("pdts",AllProducts);
		return "categoriestraditional";
	}

}
