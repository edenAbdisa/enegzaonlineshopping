package com.AADE.inigza.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.AADE.inigza.security.Role;
import lombok.Data;
@Data
@Entity
@Table(name="product_color_size",schema="inigza")
public class ProductSizeColor  {	
		@Id	
		@Column(name="Id")
		@NotBlank(message="Id is required")
		private long Id;	
		
	    @Column(name="free_shipping")
		@NotBlank(message="free shipping status required")
		private String freeShipping;	
		 	
	    @Column(name="color_id")
		@NotBlank(message="colorId is required")
		private long colorId;		 
				 
		@Column(name="size_id")
		private long sizeId;
		
		@Column(name="category_id")
		private long categoryId;
		
		@Column(name="product_id_name")
		@NotBlank(message="Item name is required")
		private String productIdName;
		
		@Column(name="Image")
		@NotBlank(message="Item name is required")
		private String Image;
		
		@Column(name="amount")
		private long amount;
		
		@Column(name="amount_in_stock")
		@NotBlank(message="PurchasingPrice is required")
		private long amountInStock;
		
		@Column(name="selling_price")
		@NotBlank(message="Selling Price is required")
		private double sellingPrice;
		
		@Column(name="initial_selling_price")
		@NotBlank(message="Selling Price is required")
		private double initialSellingPrice;
		
}
