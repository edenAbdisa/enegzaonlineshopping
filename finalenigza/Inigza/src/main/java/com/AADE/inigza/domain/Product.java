package com.AADE.inigza.domain;


import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;
import javax.validation.constraints.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data

@Entity
 public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	@NotNull(message="Name must not be empty")
	@Size(min=5,message="Name must be atleast 5 characters long")
	private  String name;
	@NotNull(message="Purchasing price must not be empty")
	private  double purchasing_price;
	@NotNull(message="selling price must not be empty")
	private  double selling_price;
	private  String materialType;
	@NotNull(message="Description must not be empty")
	private  String description;
	@NotNull(message="Category must not be empty")
	private  int category_id;
	@NotNull(message="Date must not be empty")
	private  String date;
	@NotNull(message="Gender must not be empty")
	private  String gender;
	@NotNull(message="Picture must not be empty")
	private  String picture;
	@NotNull(message="Picture must not be empty")
	private  String free_shipping;
	@NotNull(message="Picture must not be empty")
	private  String amount_in_stock;


	 
	 
	    
	

}
