package com.AADE.inigza.respositories;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.domain.Size;

public interface SizeRepository extends CrudRepository<Size,Long> {
	Size findBySizeName(String sizeName);
}
