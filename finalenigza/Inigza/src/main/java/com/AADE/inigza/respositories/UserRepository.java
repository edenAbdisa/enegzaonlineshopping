package com.AADE.inigza.respositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.security.User;

public interface UserRepository extends CrudRepository<User, Long>{

	User findByUsername(String username);
	List<User> findUserByUsername(String username);
}
