package com.AADE.inigza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.AADE.inigza.domain.*;

public interface ProductSizeColorService {
public ProductSizeColor save(ProductSizeColor role);
	
	public Iterable<ProductSizeColor> saveAll(Iterable<ProductSizeColor> color);

	Optional<ProductSizeColor> findById(int id);

	boolean existsById(Long id);
	
	Iterable<ProductSizeColor> findAll();

	Iterable<ProductSizeColor> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(ProductSizeColor role);
	
	void deleteAll(Iterable<ProductSizeColor> roles);

	void deleteAll();
	List<ProductSizeColor> findByProductIdName(String ProductSizeColor);
	List<ProductSizeColor> findByProductIdNameAndColorId(String ProductSizeColor,long colorId);
	List<ProductSizeColor> findProductSizeColorByAmountInStockGreaterThan(long value);
	ProductSizeColor findByProductIdNameAndColorIdAndSizeId(String ProductSizeColor,long colorId,long sizeId);
	 int updateAmountInStock(@Param("productIdName") String productIdName,
	    		@Param("colorId") long colorId,
	    		@Param("sizeId") long sizeId,
	    		@Param("amountInStock") long amountInStock);
	 
	 ProductSizeColor saveProductSizeColor(ProductSizeColor pds,Product pdt, Size size, Color color);

	Optional<ProductSizeColor> findById(Long id);
}
