package com.AADE.inigza.services;

import java.util.Optional;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.respositories.ColorRepository;
import com.AADE.inigza.respositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ColorServiceImpl implements ColorService {

ColorRepository colorRepository;
	
	@Autowired
	public ColorServiceImpl(ColorRepository colorRepository) {
		this.colorRepository = colorRepository;
	}
	
	@Override
	public Color save(Color color) {
		// TODO Auto-generated method stub
		return colorRepository.save(color);
	}

	@Override
	public Iterable<Color> saveAll(Iterable<Color> colors) {
		// TODO Auto-generated method stub
		return colorRepository.saveAll(colors);
	}

	@Override
	public Optional<Color> findById(Long id) {
		// TODO Auto-generated method stub
		return colorRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return colorRepository.existsById(id);
	}

	@Override
	public Iterable<Color> findAll() {
		// TODO Auto-generated method stub
		return colorRepository.findAll();
	}

	@Override
	public Iterable<Color> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return colorRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return colorRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		colorRepository.deleteById(id);
	}

	@Override
	public void delete(Color color) {
		// TODO Auto-generated method stub
		colorRepository.delete(color);
	}

	@Override
	public void deleteAll(Iterable<Color> color) {
		// TODO Auto-generated method stub
		colorRepository.deleteAll();
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		colorRepository.deleteAll();
	}

}
