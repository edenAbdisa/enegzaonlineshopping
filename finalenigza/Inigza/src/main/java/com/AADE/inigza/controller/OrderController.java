package com.AADE.inigza.controller;


import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.AADE.inigza.domain.Order;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.ProductSizeColor;
import com.AADE.inigza.domain.ProductSizeColorr;
import com.AADE.inigza.security.User;
import com.AADE.inigza.services.OrderService;
import com.AADE.inigza.services.ProductService;
import com.AADE.inigza.services.ProductSizeColorService;
import com.AADE.inigza.services.ProductSizeColorrService;
import com.AADE.inigza.services.UserService;

import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.dynamic.DynamicType.Builder.FieldDefinition.Optional;

@Slf4j
@Controller
@SessionAttributes("order")
public class OrderController {
	
	private OrderService orderService;
	private ProductSizeColorrService productSizeColor;
	private ProductService product ;
	@Autowired
	public OrderController(OrderService orderService,ProductSizeColorrService productSizeColor,ProductService product) {
		  this.orderService=orderService;
		  this.product=product;
		  this.productSizeColor=productSizeColor;
	  }
	
	@PostMapping("/orderItem") // <2>
	public String createOrder(@Valid Order order, BindingResult bindingResult, Model model,Principal princpal) {
		log.info(order.toString());
		if (bindingResult.hasErrors()) {
			//model.addAttribute("successMessage", bindingResult.getAllErrors());
			log.info(bindingResult.getAllErrors().toString());
			return ("redirect:/single/"+order.getProductIdName());
		} else {
			ProductSizeColorr toBeUpdated= productSizeColor.findByProductIdNameAndColorIdAndSizeId(
					order.getProductIdName(),order.getColor(), order.getSize());
			order.setUserName(princpal.getName());
			order.setOrderStatus("NotPaid");
			order.setPayment(order.getAmount()*toBeUpdated.getSellingPrice());
			Order savedOrder=orderService.save(order);			
			if(savedOrder!=null) {
				ProductSizeColorr product=productSizeColor.findByProductIdNameAndColorIdAndSizeId(
						savedOrder.getProductIdName(),savedOrder.getColor(),savedOrder.getSize());
				long amount=product.getAmountInStock()-savedOrder.getAmount();
				log.info(String.valueOf(product.getAmountInStock()-savedOrder.getAmount()));
				productSizeColor.updateAmountInStock(product.getProductIdName(), product.getColorId(), product.getSizeId(), amount);
						
			}else {
				return ("redirect:/single/"+order.getProductIdName());
			}
			return "redirect:/";
          //  model.addAttribute("successMessage", "User has been registered successfully");
            
			//model.addAttribute("displayedWhenAccountIsSuccesfullyCreated", "User has been created successfully");
			
		}
		
	}
	@GetMapping("/order/notpaid")
	public String showDashBoard(Model model) {
		String orderStatus = "NotPaid";
		List<Order> AllOrders = orderService.findByOrderStatus(orderStatus);
		model.addAttribute("orders",AllOrders);
		return "NotPaid";
	}
	@GetMapping("/user/order")
	public String showUserOrder(Model model,Principal principal) {	
		List<Order> AllOrders = orderService.findByUserName(principal.getName());
		model.addAttribute("orders",AllOrders);
		return "OrderUser";
	}
	@GetMapping("/order/delivered")
	public String showDeliveredDashBoard(Model model) {
		String orderStatus = "Delivered";
		List<Order> AllOrders = orderService.findByOrderStatus(orderStatus);
		model.addAttribute("orders",AllOrders);
		return "NotPaid";
	}
	@GetMapping("/order/transaction")
	public String showOntransBoard(Model model) {
		String orderStatus = "OnTrans";
		List<Order> AllOrders = orderService.findByOrderStatus(orderStatus);
		model.addAttribute("orders",AllOrders);
		return "NotPaid";
	}
	@GetMapping("/order/paid")
	public String showPaidDashBoard(Model model) {
		String orderStatus = "Paid";
		List<Order> AllOrders = orderService.findByOrderStatus(orderStatus);
		model.addAttribute("orders",AllOrders);
		return "NotPaid";
	}
	@GetMapping("/allorders")
	public String showOrderBoard(Model model) {
		return "AdminOrder";
	}
	
	@PostMapping("/order/change")
	public String processEditProduct(Order pdt) {	
		orderService.save(pdt);
		return "NotPaid";
	}
	@GetMapping("/deleteorder")
	public String deleteProduct(Model model,@RequestParam("Id")Long id) {
		orderService.deleteById(id);
		return "redirect:/user/order";
	}
}
