package com.AADE.inigza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.ProducDetails;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.ProductSizeColor;
import com.AADE.inigza.domain.ProductSizeColorr;
import com.AADE.inigza.domain.Size;
import com.AADE.inigza.respositories.ProductSizeColorRepositories;
import com.AADE.inigza.respositories.ProductSizeColorrRepository;

@Service
public class ProductSizeColorServiceImpl implements ProductSizeColorrService  {
	ProductSizeColorrRepository  productSizeColorRepositories;
	public ProductSizeColorServiceImpl(ProductSizeColorrRepository  productSizeColorRepositories) {
		this.productSizeColorRepositories=productSizeColorRepositories;
	}
	@Override
	public Iterable<ProductSizeColorr> saveAll(Iterable<ProductSizeColorr> color) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ProductSizeColorr> findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<ProductSizeColorr> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(ProductSizeColorr role) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<ProductSizeColorr> roles) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProductSizeColorr saveProductSizeColor(ProducDetails pd, int color, int size,int amount,int amount_in_stock,String image,double selling_price) {
		ProductSizeColorr pdc = new ProductSizeColorr();
		pdc.setColorId(color);
		pdc.setProductID(pd.getProductID());
		pdc.setProductIdName(pd.getProductIdName());
		pdc.setSizeId(size);
		pdc.setAmount(amount);
		pdc.setAmountInStock(amount_in_stock);
		pdc.setImage(image);
		pdc.setSellingPrice(selling_price);
		return productSizeColorRepositories.save(pdc);
		
	}
	@Override
	public List<ProductSizeColorr> findByProductIdName(String ProductSizeColor) {
		return this.productSizeColorRepositories.findByProductIdName(ProductSizeColor);
	}

	@Override
	public List<ProductSizeColorr> findProductSizeColorByAmountInStockGreaterThan(long value) {
		return this.productSizeColorRepositories.findProductSizeColorByAmountInStockGreaterThan(value);
	}
	@Override
	public ProductSizeColorr findByProductIdNameAndColorIdAndSizeId(String ProductSizeColor, long colorId, long sizeId) {
		return productSizeColorRepositories.findByProductIdNameAndColorIdAndSizeId(ProductSizeColor, colorId, sizeId);
	}
	@Override
	public int updateAmountInStock(String productIdName, long colorId, long sizeId, long amountInStock) {
		 return productSizeColorRepositories.updateAmountInStock(productIdName, colorId, sizeId, amountInStock);
	}
	@Override
	public List<ProductSizeColorr> findByProductIdNameAndColorId(String ProductSizeColor, long colorId) {
		return this.productSizeColorRepositories.findByProductIdNameAndColorId(ProductSizeColor, colorId);
	}
	@Override
	public ProductSizeColorr saveProductSizeColor(ProductSizeColorr pds,Product pdt, Size size, Color color) {
		pds.setID(pdt.getId());
		pds.setColorId(color.getId());
		return pds;
	
	}

	

}
