package com.AADE.inigza.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="product_color",schema="inigza")
public class ProductSizeColorr {
	 	
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int ID;
	 	@Column(name="productID")
		private int productID;		  
		@Column(name="size_id")
		private long sizeId;
		@Column(name="color_id")
		private long colorId;	
		@Column(name="Image")
		private String Image;
		@Column(name="product_id_name")
		private String productIdName;
		@Column(name="amount")
		private long amount;
		
		@Column(name="amount_in_stock")
		
		private long amountInStock;
		
		@Column(name="selling_price")
		private double sellingPrice;
				 
	
}
