package com.AADE.inigza.services;

import java.util.Optional;

import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.Size;

public interface SizeService  {
	
	public Size save(Size size);
	
	public Iterable<Size> saveAll(Iterable<Size> sizes);

	Optional<Size> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Size> findAll();

	Iterable<Size> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Size sizes);
	
}
