package com.AADE.inigza.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.swing.text.html.Option;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.ProducDetails;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.ProductSizeColor;
import com.AADE.inigza.domain.ProductSizeColorr;
import com.AADE.inigza.domain.Size;
import com.AADE.inigza.services.ColorService;
import com.AADE.inigza.services.ProductService;
import com.AADE.inigza.services.ProductSizeColorService;
import com.AADE.inigza.services.ProductSizeColorrService;

import antlr.collections.List;

@Controller
public class ColorController {
	
	private int PID;
	private String PIDName;
	private  ColorService colorService;
	private  ProductService productService;
	private  ProductSizeColorrService productSizeColorrService;
	
	@Autowired
	public ColorController(ColorService colorService, ProductSizeColorrService productSizeColorrService,ProductService productService) {
		this.colorService = colorService;
		this.productSizeColorrService = productSizeColorrService;
		this.productService = productService;
		
	}
	
	
	@GetMapping("/color")
	public String getColorTab(Model model) {
		Iterable<Product> AllProducts =  productService.findAll();
		model.addAttribute("product",AllProducts);
		return "Color";
	}
	@GetMapping("/addcolor")
	public String addColor(Model model,@RequestParam("Id")int id,@RequestParam("Name")String name) {
		
		Optional<Product> pd = productService.findById(id);
		
		ProducDetails product = new ProducDetails();
		PID = id;
		PIDName = name;
		product.setProductID(id);
		product.setProductIdName(PIDName);
		model.addAttribute("product",product);
		return "AddColor";
	}
	
	@PostMapping("/addColorSize")
	public String processColorSize(ProducDetails product) 
	{
		product.setProductID(PID);
		product.setProductIdName(PIDName);
		productSizeColorrService.saveProductSizeColor(product, product.getChoosenColors(), product.getChoosenSizes(),product.getAmount(),product.getAmount_in_stock(),product.getImage(),product.getSelling_price());

		return "redirect:/color";
	}
	

}
