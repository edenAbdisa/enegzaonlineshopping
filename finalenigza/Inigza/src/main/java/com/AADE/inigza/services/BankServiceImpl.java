package com.AADE.inigza.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.Bank;
import com.AADE.inigza.respositories.BankRepository;

@Service
public class BankServiceImpl implements BankService {
	
	private BankRepository bankRepos;
	
	public  BankServiceImpl(BankRepository bankRepos) {
		this.bankRepos = bankRepos;	
	}

	@Override
	public Optional<Bank> findById(int id) {
		return bankRepos.findById(id);
	}

	@Override
	public Optional<Bank> findByPincode(String pinCode) {
		return bankRepos.findByPincode(pinCode);
	}

	@Override
	public boolean existsById(int id) {
		return bankRepos.existsById(id);
	}

	@Override
	public Iterable<Bank> findAll() {
		return bankRepos.findAll();
	}

	@Override
	public Iterable<Bank> findAllById(Iterable<Integer> ids) {
		return bankRepos.findAllById(ids);
	}

}
