package com.AADE.inigza.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.Cart;
import com.AADE.inigza.domain.Wishlist;
import com.AADE.inigza.respositories.CartRepository;
import com.AADE.inigza.respositories.ColorRepository;
import com.AADE.inigza.security.User;

@Service
public class CartServiceImpl implements CartService {

	CartRepository cartRepository;
	
	@Autowired
	public CartServiceImpl(CartRepository cartRepository) {
		this.cartRepository = cartRepository;
	}
	@Override
	public Cart save(Cart cart) {
		// TODO Auto-generated method stub
		return cartRepository.save(cart);
	}

	@Override
	public Iterable<Cart> saveAll(Iterable<Cart> carts) {
		// TODO Auto-generated method stub
		return cartRepository.saveAll(carts);
	}

	@Override
	public Optional<Cart> findById(Long id) {
		// TODO Auto-generated method stub
		return cartRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return cartRepository.existsById(id);
	}

	@Override
	public Iterable<Cart> findAll() {
		// TODO Auto-generated method stub
		return cartRepository.findAll();
	}

	@Override
	public Iterable<Cart> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return cartRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return cartRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		cartRepository.deleteById(id);
	}

	@Override
	public void delete(Cart cart) {
		// TODO Auto-generated method stub
		cartRepository.delete(cart);
	}
	@Override
	public Cart saveCart(String pid, User user) {
		Cart cart = new Cart();
		cart.setUser_id(user.getId());
		cart.setProduct_id(pid);
		return cartRepository.save(cart); 
	}

}
