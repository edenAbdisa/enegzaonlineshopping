package com.AADE.inigza.respositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.domain.Order;
import com.AADE.inigza.domain.Wishlist;

public interface WishlistRepository extends CrudRepository<Wishlist,Long> {
	List<Wishlist> findByUserID(Long username);
}
