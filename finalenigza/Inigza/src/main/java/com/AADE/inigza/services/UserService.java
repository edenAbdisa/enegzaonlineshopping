package com.AADE.inigza.services;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.AADE.inigza.domain.Wishlist;
import com.AADE.inigza.security.*;


public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	List<User> findByUsername(String username);
	void saveUser(User user);
	public Iterable<User> saveAll(Iterable<User> user);

	Optional<User> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<User> findAll();
	

	Iterable<User> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(User wishlist);

}
