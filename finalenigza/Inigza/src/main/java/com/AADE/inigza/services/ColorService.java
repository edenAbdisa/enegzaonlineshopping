package com.AADE.inigza.services;


import java.util.Optional;

import com.AADE.inigza.domain.Color;


public interface ColorService {

	public Color save(Color color);
	
	public Iterable<Color> saveAll(Iterable<Color> colors);

	Optional<Color> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Color> findAll();

	Iterable<Color> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Color Color);
	
	void deleteAll(Iterable<Color> color);

	void deleteAll();
}
