package com.AADE.inigza.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Entity
public class Bank {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bankID;
	@NotNull(message="Bank number can not be empty")
	private String bankNumber;
	@NotNull(message="Pin code can not be empty")
	private String pincode;
	@NotNull(message="Bank number can not be empty")
	private String UserID;
	
	
	
}
