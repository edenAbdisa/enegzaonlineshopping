package com.AADE.inigza.respositories;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.security.Role;;


public interface RoleRepository extends CrudRepository<Role, Long> {

	public Role findByRole(String string);

}
