package com.AADE.inigza.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("admin")
public class AdminController {
	
	@GetMapping("/admin")
	public String showDashBoard(Model model,Principal principal) {
		model.addAttribute("admin",principal.getName());
		return "admin";
	}

}
