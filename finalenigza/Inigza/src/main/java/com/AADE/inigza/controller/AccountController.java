package com.AADE.inigza.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.*;

import javax.validation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.AADE.inigza.domain.Product;
import com.AADE.inigza.security.User;
import com.AADE.inigza.respositories.UserRepository;
import com.AADE.inigza.services.UserService;



import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import lombok.extern.slf4j.Slf4j;

@Controller
public class AccountController {
	
	@Autowired
    private UserService userService;
	
	@PostMapping("/getPath")
	public String getPath(Model model) {
		model.addAttribute("user",new User());
		return "account";
	}
	
	
	@GetMapping("/account")
	public String showAccountPage(Model model) {
		model.addAttribute("user",new User());
		return "account";
	}
	
	@PostMapping("/createAccount")
	public String processRegistration(@Valid User user, BindingResult bindingResult, Model model) {
		
		User userExists = userService.findUserByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("username", "error.username",
                            "There is already a user registered with the username provided");
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/account";
        } else {
        	
            userService.saveUser(user);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "redirect:/";
        }
		
	}
	
	@GetMapping("/login")     
	  public String login() {
		
	    return "login";   
	 }
	
	@PostMapping("/login")
	  public String logIn(User user,Model model) {	
		model.addAttribute("user",user);
		return "redirect:/";
	 }
	
	
	
	
	
	
	
	

	
}
