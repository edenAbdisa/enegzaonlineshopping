package com.AADE.inigza.services;

import java.util.List;
import java.util.Optional;

import com.AADE.inigza.domain.Cart;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.Wishlist;
import com.AADE.inigza.security.User;

	public interface WishlistService {
	
	public Wishlist save(Wishlist wishlist);
	
	public Iterable<Wishlist> saveAll(Iterable<Wishlist> wishlist);

	Optional<Wishlist> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Wishlist> findAll();
	

	Iterable<Wishlist> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Wishlist wishlist);
	
	public Wishlist saveWishlist(String pid, User user);
	List<Wishlist> findByUserID(Long username);
}
