package com.AADE.inigza.respositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.domain.Bank;

public interface BankRepository extends CrudRepository<Bank,Integer> {
	
	Optional<Bank> findByPincode(String pinCode);
}
