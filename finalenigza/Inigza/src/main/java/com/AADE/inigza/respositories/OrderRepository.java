package com.AADE.inigza.respositories;

import java.util.Iterator;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.domain.Order;
import com.AADE.inigza.domain.ProductSizeColorr;
import com.AADE.inigza.domain.Size;



 
public interface OrderRepository extends CrudRepository<Order, Long> {
	
	
	List<Order> findByOrderStatus(String orderStatus);
	List<Order> findByUserName(String username);
	
}
