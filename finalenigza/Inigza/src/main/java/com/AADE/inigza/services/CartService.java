package com.AADE.inigza.services;

import java.util.Optional;

import com.AADE.inigza.domain.Cart;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.Wishlist;
import com.AADE.inigza.security.User;

public interface CartService {

	public Cart save(Cart cart);
	
	public Iterable<Cart> saveAll(Iterable<Cart> carts);

	Optional<Cart> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Cart> findAll();
	

	Iterable<Cart> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Cart cart);
	 Cart saveCart(String pid, User user);

}
