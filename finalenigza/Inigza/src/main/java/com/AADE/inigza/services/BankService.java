package com.AADE.inigza.services;

import java.util.Optional;



import com.AADE.inigza.domain.Bank;



public interface BankService {
	

	Optional<Bank> findById(int id);
	
	Optional<Bank> findByPincode(String pinCode);

	boolean existsById(int id);
	
	Iterable<Bank> findAll();

	Iterable<Bank> findAllById(Iterable<Integer> ids);

	
	
	
	
}
