package com.AADE.inigza.services;

import java.util.List;
import java.util.Optional;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.security.*;;

public interface ProductService {
	
	public Product save(Product product);
	
	public Iterable<Product> saveAll(Iterable<Product> products);

	Optional<Product> findById(int id);

	boolean existsById(int id);
	
	Iterable<Product> findAll();
	
	Iterable<Product> findByGender(String Gender);

	Iterable<Product> findAllById(Iterable<Integer> ids);

	long count();
	
	void deleteById(int id);
	
	void delete(Product product);
	

	
	void deleteAll(Iterable<Product> products);

	void deleteAll();

	public List<Product> findByProductIdName(String id);
}
