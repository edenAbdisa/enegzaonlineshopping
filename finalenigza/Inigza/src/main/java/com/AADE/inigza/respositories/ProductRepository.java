package com.AADE.inigza.respositories;




import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.Product;


public interface ProductRepository extends CrudRepository<Product,Integer>,PagingAndSortingRepository<Product, Integer> {

	Iterable<Product> findByGender(String Gender);
	 List<Product> findByName(String id);
}
