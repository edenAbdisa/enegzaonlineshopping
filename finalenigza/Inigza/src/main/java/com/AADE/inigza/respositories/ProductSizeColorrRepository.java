package com.AADE.inigza.respositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.AADE.inigza.domain.ProductSizeColor;
import com.AADE.inigza.domain.ProductSizeColorr;

public interface ProductSizeColorrRepository extends CrudRepository<ProductSizeColorr,Integer> {
	ProductSizeColorr findByProductIdNameAndColorIdAndSizeId(String ProductSizeColor,long colorId,long sizeId);
	List<ProductSizeColorr> findByProductIdNameAndColorId(String ProductSizeColor,long colorId);
	List<ProductSizeColorr> findByProductIdName(String ProductSizeColor);
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE ProductSizeColor c SET c.amountInStock = :amountInStock WHERE c.colorId = :colorId AND c.sizeId= :sizeId AND c.productIdName= :productIdName")
    int updateAmountInStock(@Param("productIdName") String productIdName,
    		@Param("colorId") long colorId,
    		@Param("sizeId") long sizeId,
    		@Param("amountInStock") long amountInStock);
	//@Query("ProductSizeColor p where p.amountInStock >0")
	List<ProductSizeColorr> findProductSizeColorByAmountInStockGreaterThan(long value);
	 
}
