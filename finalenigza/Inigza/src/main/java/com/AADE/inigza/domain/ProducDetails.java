package com.AADE.inigza.domain;




public class ProducDetails {
	private int productID;
	private int choosenColors;
	private int choosenSizes;
	private String productIdName;
	private int amount;
	private int amount_in_stock;
	private String image;
	private double selling_price;
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public int getChoosenColors() {
		return choosenColors;
	}
	public void setChoosenColors(int choosenColors) {
		this.choosenColors = choosenColors;
	}
	public int getChoosenSizes() {
		return choosenSizes;
	}
	public void setChoosenSizes(int choosenSizes) {
		this.choosenSizes = choosenSizes;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getAmount_in_stock() {
		return amount_in_stock;
	}
	public void setAmount_in_stock(int amount_in_stock) {
		this.amount_in_stock = amount_in_stock;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public double getSelling_price() {
		return selling_price;
	}
	public void setSelling_price(double selling_price) {
		this.selling_price = selling_price;
	}
	public String getProductIdName() {
		return productIdName;
	}
	public void setProductIdName(String productIdName) {
		this.productIdName = productIdName;
	}
	

}
