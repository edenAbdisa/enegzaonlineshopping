package com.AADE.inigza.respositories;

import org.springframework.data.repository.CrudRepository;

import com.AADE.inigza.domain.Cart;

public interface CartRepository extends CrudRepository<Cart,Long> {

}
