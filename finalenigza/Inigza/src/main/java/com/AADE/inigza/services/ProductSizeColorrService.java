package com.AADE.inigza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.AADE.inigza.domain.Color;
import com.AADE.inigza.domain.ProducDetails;
import com.AADE.inigza.domain.Product;
import com.AADE.inigza.domain.ProductSizeColor;
import com.AADE.inigza.domain.ProductSizeColorr;
import com.AADE.inigza.domain.Size;


public interface ProductSizeColorrService {
	
	public Iterable<ProductSizeColorr> saveAll(Iterable<ProductSizeColorr> color);

	Optional<ProductSizeColorr> findById(int id);

	boolean existsById(int id);
	
	Iterable<ProductSizeColorr> findAll();



	long count();
	
	void deleteById(int id);
	
	void delete(ProductSizeColorr role);
	
	void deleteAll(Iterable<ProductSizeColorr> roles);

	void deleteAll();
	
	ProductSizeColorr saveProductSizeColor(ProducDetails pd, int Color, int size,int amount,int amount_in_stock,String image,double selling_price);
	List<ProductSizeColorr> findByProductIdName(String ProductSizeColorr);
	List<ProductSizeColorr> findByProductIdNameAndColorId(String ProductSizeColorr,long colorId);
	List<ProductSizeColorr> findProductSizeColorByAmountInStockGreaterThan(long value);
	ProductSizeColorr findByProductIdNameAndColorIdAndSizeId(String ProductSizeColorr,long colorId,long sizeId);
	 int updateAmountInStock(@Param("productIdName") String productIdName,
	    		@Param("colorId") long colorId,
	    		@Param("sizeId") long sizeId,
	    		@Param("amountInStock") long amountInStock);
	 
	 ProductSizeColorr saveProductSizeColor(ProductSizeColorr pds,Product pdt, Size size, Color color);

}
