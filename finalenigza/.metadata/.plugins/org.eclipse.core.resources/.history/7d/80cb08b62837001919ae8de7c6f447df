package com.AADE.inigza.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
	}
	 @Override
	  protected void configure(HttpSecurity http) throws Exception {
		 http.authorizeRequests()
	     .antMatchers("/").permitAll()
	     .antMatchers("/login").permitAll()
	     .antMatchers("/account").permitAll()
	     .antMatchers("/createAccount").permitAll()
	     
	     .antMatchers("/admin").hasRole("ADMIN")
	     .anyRequest().authenticated()
	     .and()
	     	.formLogin()
	     			.loginPage("/login")
	     			.failureUrl("/login?error=true")
	     			.defaultSuccessUrl("/")
	     .and()
	     	.logout()
	     			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	     			.logoutSuccessUrl("/")
     	.and()
	     	.exceptionHandling()
	     	.accessDeniedPage("/access-denied");
	  }
	@Bean
	  public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder();
	  }
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**","/styles/**","/plugins/**","/assets/**");
		 
	 }
	
	
}

