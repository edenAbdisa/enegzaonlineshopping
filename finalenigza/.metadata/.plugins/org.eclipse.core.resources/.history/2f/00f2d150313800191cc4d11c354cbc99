package com.AADE.inigza.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	 
    @Autowired
    public SecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }
	@Autowired
	private UserDetailsService userDetailsService;
	@Override
	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
	}
	 @Override
	  protected void configure(HttpSecurity http) throws Exception {
		 http.authorizeRequests()
	     .antMatchers("/").permitAll()
	     .antMatchers("/login").permitAll()
	     .antMatchers("/account").permitAll()
	     .antMatchers("/createAccount").permitAll()
	     .antMatchers("/categorieswomen").permitAll()
	     .antMatchers("/categoriesmen").permitAll()
	     .antMatchers("/single/**").permitAll()
	     .antMatchers("/wishlist/**").permitAll()
	     .antMatchers("/orderItem/**").permitAll()
	     .antMatchers("/admin").hasRole("ADMIN")
	     .anyRequest().authenticated()
	     .and()
	     	.formLogin()
	     			.loginPage("/login")
	     			.loginProcessingUrl("/login")
	     			.usernameParameter("username")
	     			.passwordParameter("password")
	     			.successHandler(authenticationSuccessHandler)
	     			.failureUrl("/login?error=true")
	     			.and()
	     			.logout()
	     			.logoutUrl("/logout")
	     			.logoutSuccessUrl("/")
	     			.and().exceptionHandling()
	     			.accessDeniedPage("/access_denied")
	     			
	     			
	     			
	     .and()
	     	.logout()
	     			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	     			.logoutSuccessUrl("/")
     	.and()
	     	.exceptionHandling()
	     	.accessDeniedPage("/access-denied");
	  }
	@Bean
	  public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder();
	  }
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**","/styles/**","/plugins/**","/assets/**");
		 
	 }
	
	
}

