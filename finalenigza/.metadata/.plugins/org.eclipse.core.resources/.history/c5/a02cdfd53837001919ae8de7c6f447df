package com.AADE.inigza.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import antlr.collections.List;
import lombok.Data;

@Entity
@Data
@Table(name = "user")
public class User implements UserDetails{
	private static final String ROLE_ADMIN = "ROLE_ADMIN";
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;		 
	
	@NotBlank(message = "Please provide a username")
	private  String username;
	
	@Column(name="phone_number")
	@NotBlank(message="Phone number is required")
	private  String phoneNumber;
	
	@Column(name="email")
	@NotBlank(message="Email is required")
	private  String email;
	
	@Size(min = 5, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")		
	private String password;
	
	@Size(min = 3, message = "Please Enter your name")
    @NotBlank(message = "Your first name can't be empty")
	@Column(name="first_name")
	private  String firstName;
	
	@Size(min = 1, message = "Please Enter your name")
    @NotBlank(message = "Your Last name can't be empty")
	@Column(name="last_name")
	private  String lastName;
	
	
	@Column(name="optional_phone_number")
	private  String optionalPhoneNumber;
	
	 @Column(name = "enabled")
	    private int enabled;
	 @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	  @JoinTable(name="user_role", 
	    			joinColumns= {@JoinColumn(name="user_id")},
	    			inverseJoinColumns= {@JoinColumn(name="role_id")})
	    private Set<Role> roles;
	
	 /*@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	 @JoinColumn(name="username")
	 private Set<Order> order;*/
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List list = null;
	if(roles.contains(ROLE_ADMIN)) {
		list = (List) Arrays.asList(new SimpleGrantedAuthority(ROLE_ADMIN));
	}
	else if(roles.contains("ROLE_USER")){
		list = (List) Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}
	return (Collection<? extends GrantedAuthority>) list;
	}
	@Override
	public boolean isAccountNonExpired() {
	return true;
	}
	@Override
	public boolean isAccountNonLocked() {
	return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
	return true;
	}
	@Override
	public boolean isEnabled() {
	return true;
	}
	

}
